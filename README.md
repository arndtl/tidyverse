# Introduction to the `tidyverse`

11. November 2024, Methodenkompetenzzentrum der Technischen Universität Chemnitz

## Kursbeschreibung

Die Fähigkeit, komplexe Datenstrukturen zu handhaben und empirische Ergebnisse zu visualisieren, ist für Sozialwissenschaftler:innen eine zunehmend wichtige Fertigkeit. Dieser Kurs führt Sie in die Pakete des `tidyverse` ein. Er behandelt außerdem die Prinzipien von "tidy data" und der "grammar of graphics", die die konzeptionelle Basis für `dplyr`, `tidyr` und `ggplot2` bilden. Das Ziel des Workshops ist es, Ihnen das grundlegende Wissen zu vermitteln, das Sie benötigen, um die `tidyverse`-Pakete eigenständig zu nutzen und so als Teilnehmende Ihren eigenen Lernprozess zu initiieren und die spezifischen Fähigkeiten zu erwerben, die Sie für Ihr Studium und Ihre Forschung benötigen.

## Zielgruppe

Der Workshop richtet sich an Studierende und Promovierende, die bereits über erste Erfahrungen mit R und RStudio verfügen.

## Kursinhalte und Lernziele

Der Workshop umfasst einige strukturierte Vorträge, legt aber den Schwerpunkt auf praktische Übungen. Für jede Übung werden Daten und Code zur Verfügung gestellt, aber die Teilnehmenden sind eingeladen, ihre eigenen Daten zu verwenden.

Der Workshop wird folgende Themen umfassen:

- Dokumente und Präsentationen mit Quarto
   - Rekapitulation von Markdown und Integration von R-Code
   - Besondere Eigenschaften von Quarto im Vergleich zu Rmarkdown
- Tidyverse
   - Piping
   - Grundlegende Funktionen
   - Zusammenführen von Datensätzen
   - Konvertierung von Daten vom `wide`- ins `long`-Format und umgekehrt
- Datenvisualisierung mit ggplot2
   - Grundlagen der Datenvisualisierung
   - Koeffizienten-Diagramme

## Voraussetzungen und Vorbereitungen

You will use your own laptops as R and RStudio are available free of charge for all major operating systems -- one of the many benefits of R. You will be expected to have installed R and RStudio prior to coming to the workshop.

Please do the following before the workshop:

1. Download all course material by either cloning this repository using Git or downloading it manually. For the latter click on the button containing a downward pointing arrow next to the button "Clone".
2. Run code in `install_packages.R` to install all packages that will be used in this workshop.

## Kursprogramm

### Montag, 11. November 2024

**10:00--11:00 Uhr** Documents and presentations with Quarto 

**11:00--11:30 Uhr** *Kaffeepause*

**11:30--13:00 Uhr** Tidyverse

**13:00--14:00 Uhr** *Mittagspause*

**14:00--15:30 Uhr** Visualizing data with ggplot2 

**15:30--16:00 Uhr** *Kaffeepause*

**16:00--17:00 Uhr** Visualizing data with ggplot2

## Ihr Dozent

Arndt Leininger ist Juniorprofessor für Politikwissenschaftliche Forschungsmethoden an der Technischen Universität Chemnitz, wo er eine Emmy Noether-Forschungsgruppe zu dem Thema „Polarisierung durch und in Referenden: Polarisierung innerhalb und außerhalb des Parteiensystems“ leitet. Seine Forschung konzentriert sich auf die Wahl- und Einstellungsforschung unter Verwendung quantitative Methoden. Er interessiert sich für Rechtspopulismus, Polarisierung, direkte Demokratie, Wahlbeteiligung, Jugendliche in der Politik, Wahlprognosen, ökonomisches Wählen und Wahlstudien im Allgemeinen. Seine Forschung wurde unter anderem in den Fachzeitschriften *American Political Science Review*, *Political Science Research and Methods*, *Political Studies*, *Politische Vierteljahresschrift* und *West European Politics* veröffentlicht. Arndt unterrichtet regelmäßig Kurse zur Anwendung von R für akademische Forschung.